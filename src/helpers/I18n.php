<?php

namespace code2magic\i18n\helpers;

use code2magic\i18n\backend\models\Source;
use code2magic\i18n\backend\models\Translation;
use yii\helpers\ArrayHelper;

/**
 * Class I18n
 * @package code2magic\i18n\helpers
 */
class I18n
{
    /**
     * @param \yii\i18n\MissingTranslationEvent $event
     */
    public static function missingTranslation($event)
    {
        $source = Source::find()->where(['message' => $event->message, 'category' => $event->category])->one();
        if (!$source) {
            $source = new Source(['message' => $event->message, 'category' => $event->category]);
            $source->save();
        }
        $messages = Translation::find()->where(['id' => $source->id,])->indexBy('language')->all();
        foreach (Locale::getLanguages() as $code => $name) {
            if(
                !($message = ArrayHelper::getValue($messages, $code, false))
                && empty($message->translation)
            ) {
                $message = $message ?: new Translation(['id' => $source->id, 'language' => $code]);
                $message->translation = '';
                $message->save();
            }
        }
    }
}
