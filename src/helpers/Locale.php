<?php

namespace code2magic\i18n\helpers;

use code2magic\i18n\models\Language;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;

/**
 * Class Locale
 *
 * @package code2magic\i18n\helpers
 */
class Locale
{
    private static $_languages = [];
    private static $_language_links_data = [];

    /**
     * @return mixed
     */
    public static function getLanguages($active_only = false)
    {
        if (!self::$_languages) {
            self::$_languages = Language::find()
                ->select('name')
                ->indexBy('code')
                ->where($active_only ? ['status' => 1,] : [])
                ->asArray()
                ->cache(true, new TagDependency(['tags' => Language::class]))
                ->column();
        }
        return self::$_languages;
    }

    /**
     * @return array
     */
    public static function getLanguageLinksData($scheme = true)
    {
        if (!self::$_language_links_data) {
            $route = \Yii::$app->controller->route;
            $params = \Yii::$app->request->queryParams;
            array_unshift($params, '/' . $route);
            $languages = \Yii::$app->urlManager->languages;

            foreach ($languages as $language) {
                $isCurrent = Locale::checkIsCurrentLanguage($language);
                $params['language'] = $language;
                $url = Url::toRoute($params, $scheme);
                self::$_language_links_data[$language] = [
                    'label' => ArrayHelper::getValue(static::getLanguages(), $language, $language),
                    'url' => $url,
                    'active' => $isCurrent,
                ];
            }
        }
        return self::$_language_links_data;
    }

    /**
     * @param $view View
     */
    public static function registerLangLinkTag($view)
    {
        foreach (static::getLanguageLinksData() as $language_code => $item) {
            $view->registerLinkTag(['rel' => 'alternate', 'hreflang' => $language_code, 'href' => $item['url'],]);
        }
    }

    /**
     * @return bool
     */
    public static function checkIsCurrentLanguage($language)
    {
        $current_language = \Yii::$app->language;
        $isWildcard = substr($language, -2) === '-*';
        if ($isWildcard) {
            $language = substr($language, 0, 2);
        }
        return ($language === $current_language || ($isWildcard && substr($current_language, 0, 2) === $language));
    }
}
