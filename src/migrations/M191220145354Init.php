<?php

namespace code2magic\i18n\migrations;

use yii\db\Migration;
use yii\helpers\Console;

/**
 * Class M191220145354Init
 */
class M191220145354Init extends Migration
{
    /**
     * @var null|string
     */
    protected $tableOptions;

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // language
        if (!$this->checkIsTableExists('{{language%}}', 'exists')) {
            $this->createTable('{{%language}}', [
                'code' => $this->string(16)->notNull()->append('COLLATE "utf8_general_ci"'),
                'name' => $this->string()->notNull()->defaultValue(''),
                'status' => $this->smallInteger()->notNull()->defaultValue(1),
                'sort' => $this->smallInteger()->notNull()->defaultValue(1),
            ], $this->tableOptions);
            $this->addPrimaryKey('pk-language', '{{%language}}', ['code',]);
            $this->batchInsert(
                '{{%language}}',
                ['code', 'name', 'status', 'sort'],
                [
                    ['en', 'English', 1, 1,],
                    ['uk', 'Українська', 1, 2,],
                    ['ru', 'Русский', 1, 3,],
                ]
            );
        }
        // i18n_source_message
        if (!$this->checkIsTableExists('{{%i18n_source_message}}', 'exists')) {
            $this->createTable('{{%i18n_source_message}}', [
                'id' => $this->primaryKey(),
                'category' => $this->string(),
                'message' => $this->text()
            ], $this->tableOptions);
            $this->createIndex('idx-source_message-category', '{{%i18n_source_message}}', 'category');
        }
        // i18n_message
        if (!$this->checkIsTableExists('{{%i18n_message}}', 'exists')) {
            $this->createTable('{{%i18n_message}}', [
                'id' => $this->integer(),
                'language' => $this->string(16)->notNull()->append('COLLATE "utf8_general_ci"'),
                'translation' => $this->text()
            ], $this->tableOptions);
            $this->addPrimaryKey('pk-i18n_message', '{{%i18n_message}}', ['id', 'language',]);
            $this->addForeignKey(
                'fk-i18n_message-2-i18n_source_message',
                '{{%i18n_message}}', 'id',
                '{{%i18n_source_message}}', 'id',
                'cascade', 'restrict'
            );
            $this->addForeignKey(
                'fk-i18n_message-2-language',
                '{{%i18n_message}}', 'language',
                '{{%language}}', 'code',
                'cascade', 'cascade'
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // i18n_message
        if ($this->checkIsTableExists('{{%i18n_message}}', 'not exists')) {
            $this->dropForeignKey('fk-i18n_message-2-language', '{{%i18n_message}}');
            $this->dropForeignKey('fk-i18n_message-2-i18n_source_message', '{{%i18n_message}}');
            $this->dropTable('{{%i18n_message}}');
        }
        // i18n_source_message
        if ($this->checkIsTableExists('{{%i18n_source_message}}', 'not exists')) {
            $this->dropIndex('idx-source_message-category', '{{%i18n_source_message}}');
            $this->dropTable('{{%i18n_source_message}}');
        }
        // language
        if ($this->checkIsTableExists('{{%language}}', 'not exists')) {
            $this->dropTable('{{%language}}');
        }
    }

    /**
     * @param string $tableName
     */
    protected function checkIsTableExists($tableName, $info_if = 'exists')
    {
        $output = $this->db->getTableSchema($tableName, true) !== null;
        if ($output && $info_if === 'exists') {
            Console::output(Console::ansiFormat("Table {$tableName} exists!", [Console::FG_YELLOW,]));
        } elseif (!$output && $info_if === 'not exists') {
            Console::output(Console::ansiFormat("Table {$tableName} doesn`t exists!", [Console::FG_YELLOW,]));
        }
        return $output;
    }
}
