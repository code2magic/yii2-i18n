<?php

namespace code2magic\i18n\migrations;

use yii\db\Migration;

/**
 * Class M200417080025FixI18nSourceMessageColumns
 */
class M200417080025FixI18nSourceMessageColumns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%i18n_source_message}}', 'category', $this->string()->append('COLLATE "utf8_bin"'));
        $this->alterColumn('{{%i18n_source_message}}', 'message', $this->text()->append('COLLATE "utf8_bin"'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%i18n_source_message}}', 'message', $this->text()->append('COLLATE "utf8_unicode_ci"'));
        $this->alterColumn('{{%i18n_source_message}}', 'category', $this->string()->append('COLLATE "utf8_unicode_ci"'));
    }
}
