<?php

namespace code2magic\i18n\backend\traits;


use Yii;

trait ModuleTrait
{

    /**
     * @return array
     */
    public function getLanguages()
    {
        $languages = [];
        foreach (\code2magic\i18n\helpers\Locale::getLanguages() as $locale => $name) {
            if ($locale !== Yii::$app->sourceLanguage)
                $languages[substr($locale, 0, 2)] = $name;
        }

        return $languages;
    }

}