<?php
/**
 * @var $this yii\web\View
 * @var $model code2magic\i18n\models\i18n\Language * @var $form yii\bootstrap\ActiveForm
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="language-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <?= $form->field($model, 'code') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'sort') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('i18n/backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('i18n/backend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
