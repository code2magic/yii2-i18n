<?php
/**
* @var $this yii\web\View
* @var $model code2magic\i18n\models\Language
 */

use yii\helpers\Html;

$this->title = Yii::t('i18n/backend', 'Update {modelClass}: ', [
    'modelClass' => 'Language',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('i18n/backend', 'Languages'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->code,],];
$this->params['breadcrumbs'][] = Yii::t('i18n/backend', 'Update');
?>
<div class="language-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
