<?php
/**
 * @var yii\web\View $this
 * @var code2magic\i18n\models\i18n\Language $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('i18n/backend', 'Languages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-index">
    <?php // echo $this->render('_search', ['model' => $searchModel,]); ?>
    <p>
        <?= Html::a(
            Yii::t('i18n/backend', 'Create {modelClass}', [
                'modelClass' => 'Language',
            ]),
            ['create',],
            ['class' => 'btn btn-success',]
        ) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => \yii\grid\SerialColumn::class,],
            'code',
            'name',
            'status',
            'sort',
            ['class' => \yii\grid\ActionColumn::class,],
        ],
    ]); ?>
</div>
