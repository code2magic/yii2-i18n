<?php
/**
* @var $this yii\web\View
* @var $model code2magic\i18n\models\Language*/

use yii\helpers\Html;

$this->title = Yii::t('i18n/backend', 'Create {modelClass}', [
    'modelClass' => 'Language',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('i18n/backend', 'Languages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
