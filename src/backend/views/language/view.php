<?php
/**
* @var $this yii\web\View
* @var $model code2magic\i18n\models\Language*/

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('i18n/backend', 'Languages'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-view">
    <p>
        <?= Html::a(Yii::t('i18n/backend', 'Update'), ['update', 'id' => $model->code,], ['class' => 'btn btn-primary',]) ?>
        <?= Html::a(Yii::t('i18n/backend', 'Delete'), ['delete', 'id' => $model->code,], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('i18n/backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code',
            'name',
            'status',
            'sort',
        ],
    ]) ?>
</div>
