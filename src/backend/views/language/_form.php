<?php
/**
 * @var $this yii\web\View
 * @var $model code2magic\i18n\models\Language * @var $form yii\bootstrap\ActiveForm
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="language-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'status')->checkbox() ?>
    <?= $form->field($model, 'sort')->textInput(['maxlength' => true]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('i18n/backend', 'Create') : Yii::t('i18n/backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
