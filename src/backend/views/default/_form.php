<?php
/**
 * @var yii\web\View $this
 * @var \code2magic\core\models\MultiModel $model
 * @var array $languages
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model->getModel('source'), 'category')->textInput(['maxlength' => 255]) ?>
<?= $form->field($model->getModel('source'), 'message')
    ->textarea(['disabled' => !$model->getModel('source')->getIsNewRecord(),]) ?>
<?php if (!$model->getModel('source')->isNewRecord) { ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('backend', 'Translations') ?></h3>
        </div>
        <div class="panel-body">
            <?php foreach ($languages as $language => $name) { ?>
                <?= $form->field($model->getModel($language), 'translation')->textarea([
                    'id' => $language . '-translation',
                    'name' => $language . '[translation]',
                ])->label($name) ?>
            <?php } ?>
        </div>
    </div>
<?php } ?>
<div class="form-group">
    <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>
