<?php

namespace code2magic\i18n\backend\models\search;

use code2magic\i18n\models\Language;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class LanguageSearch
 * @package code2magic\i18n\backend\models\search
 */
class LanguageSearch extends \code2magic\i18n\backend\models\Language
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name'], 'safe'],
            [['status', 'sort'], 'integer'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Language::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'status' => $this->status,
            'sort' => $this->sort,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
