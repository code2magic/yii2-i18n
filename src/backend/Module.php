<?php

namespace code2magic\i18n\backend;

use code2magic\i18n\backend\models\Source;
use code2magic\i18n\backend\models\Translation;

/**
 * translation module definition class
 */
class Module extends \yii\base\Module
{
    /** @inheritdoc */
    public $controllerNamespace = 'code2magic\i18n\backend\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
