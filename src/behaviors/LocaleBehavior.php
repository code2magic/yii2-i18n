<?php

namespace code2magic\i18n\behaviors;

use code2magic\i18n\helpers\Locale;
use Yii;
use yii\base\Behavior;
use yii\web\Application;

/**
 * Class LocaleBehavior
 * @package common\behaviors
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class LocaleBehavior extends Behavior
{
    /**
     * @var string
     */
    public $cookieName = '_locale';

    /**
     * @var bool
     */
    public $enablePreferredLanguage = true;

    /**
     * @return array
     */
    public function events()
    {
        return [
            Application::EVENT_BEFORE_REQUEST => 'beforeRequest'
        ];
    }

    /**
     * Resolve application language by checking user cookies, preferred language and profile settings
     */
    public function beforeRequest()
    {
        $hasCookie = Yii::$app->getRequest()->getCookies()->has($this->cookieName);
        $forceUpdate = Yii::$app->session->hasFlash('forceUpdateLocale');
        if ($hasCookie && !$forceUpdate) {
            $locale = Yii::$app->getRequest()->getCookies()->getValue($this->cookieName);
        } else {
            $locale = $this->resolveLocale();
        }
        Yii::$app->language = $locale;
    }

    public function resolveLocale()
    {
        if (!Yii::$app->user->isGuest && ($locale = $this->getAvailableLanguage(Yii::$app->user->identity->userProfile->locale))) {
            return $locale;
        }
        if ($this->enablePreferredLanguage) {
            return Yii::$app->getRequest()->getPreferredLanguage($this->getAvailableLanguages());
        }
        return Yii::$app->language;
    }

    /**
     * @return array
     */
    protected function getAvailableLanguages(): array
    {
        return array_keys(Locale::getLanguages());
    }

    /**
     * @param string $language
     * @return string
     */
    protected function getAvailableLanguage($language): string
    {
        $normalizedLanguage = str_replace('_', '-', strtolower($language));
        foreach ($this->getAvailableLanguages() as $availableLanguage) {
            $normalizedAvailableLanguage = str_replace('_', '-', strtolower($availableLanguage));
            if (
                $normalizedLanguage === $normalizedAvailableLanguage // en-us==en-us
                || strpos($normalizedAvailableLanguage, $normalizedLanguage . '-') === 0 // en==en-us
                || strpos($normalizedLanguage, $normalizedAvailableLanguage . '-') === 0 // en-us==en
            ) {
                return $availableLanguage;
            }
        }

        return false;
    }
}
