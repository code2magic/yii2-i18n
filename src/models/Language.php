<?php

namespace code2magic\i18n\models;

use code2magic\core\behaviors\CacheInvalidateBehavior;
use Yii;

/**
 * This is the model class for table "{{%language}}".
 *
 * @property string $code [varchar(16)]
 * @property string $name [varchar(255)]
 * @property int $status [smallint(6)]
 * @property int $sort [smallint(6)]
 */
class Language extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%language}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'cacheInvalidate' => [
                'class' => CacheInvalidateBehavior::class,
                'cacheComponent' => 'cache',
                'tags' => [
                    self::class,
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['status'], 'integer'],
            [['name', 'code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('i18n/model_labels', 'Name'),
            'code' => Yii::t('i18n/model_labels', 'Code'),
            'status' => Yii::t('i18n/model_labels', 'Status'),
        ];
    }
}
