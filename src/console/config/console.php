<?php
return [
    'controllerMap' => [
        'message' => [
            'class' => code2magic\i18n\console\controllers\ExtendedMessageController::class
        ],
        'migrate' => [
            'migrationNamespaces' => [
                'code2magic\i18n\migrations',
            ],
        ],
    ],
];
